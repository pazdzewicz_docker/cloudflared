FROM debian:bookworm

ENV CLOUDFLARE_TOKEN ""

COPY ./src/entrypoint.bash /entrypoint.bash

RUN chmod +x /entrypoint.bash && \
    apt update -y && \
    apt upgrade -y && \
    apt install -y curl && \
    curl -L --output cloudflared.deb https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64.deb && \
    dpkg -i cloudflared.deb && \
    rm -rf cloudflared.deb

ENTRYPOINT [ "/entrypoint.bash" ]